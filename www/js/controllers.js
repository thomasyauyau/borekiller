angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('RiddlesCtrl2', function($scope, $stateParams,$sce) {
  $scope.riddles = [
    { title: 'Q太郎', id: 1 ,
	content : 'Q太郎我睇住佢大 由細到大都係由佢婆婆揍大佢<br/>\
				估唔到我返黎呢度會撞到佢 佢而家都廿幾歲人喇<br/>\
				佢智商有問題既 所以養大佢真係唔係易事 真係戥佢婆婆辛苦<br/>\
				「喂 Q太郎 記唔記得我呀？以前住你附近 成日同你玩果個哥哥呀<br/>\
				佢冇理我 只係執住一塊六角形既木板走左<br/>\
				「喂 你識Q太郎架？」一個大嬸同我講野<br/>\
				我及頭回應左佢<br/>\
				「佢唔係第一次喇」<br/>\
				<br/>\
				佢講野冇頭冇尾我都唔知佢講乜<br/>\
				我無鎖事事咪就周圍行下望下羅<br/>\
				點知我諗諗下 發覺Q太郎有D唔對路<br/>\
				所以再搵個機會 響返呢度等佢<br/>\
				之後再跟蹤佢<br/>\
				去到一間爛屋度 響爛屋出面<br/>\
				我見到一塊木板 寫住兩組數字︰<br/>\
				「6+6X12+12=24」<br/>\
				「1+1+1+1+1+1+1+1」<br/>\
				頭兩個「1」字比較大<br/>\
				<br/>\
				之後我行左入佢間爛屋<br/>\
				滿地都係老鼠曱甴<br/>\
				仲發出陣陣臭味<br/>\
				我再行入裡面<br/>\
				望返轉頭已經凌黑一片…<br/>\
				當我再次見到佢…<br/>\
				我…哭了'
	},
    { title: 'Chill', id: 2 ,content : 'test' },
    { title: 'Dubstep', id: 3 ,content : 'test' },
    { title: 'Indie', id: 4 ,content : 'test' },
    { title: 'Rap', id: 5 ,content : 'test' },
    { title: 'Cowbell', id: 6,content : 'test'  }
  ];
  
  $scope.getCurrentElement = function(){
	var pageEle = {};
	angular.forEach($scope.riddles, function(value, key) {
	  if(value.id == $stateParams.riddleId){
		  pageEle = value;
	  }
	});
	return pageEle ;
  };
  
  $scope.getContent = function(){ 
	  return $sce.trustAsHtml($scope.getCurrentElement().content);
  }
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
